import React from 'react';

import Input from '../Input';

const TimeInput = (props) => (
    <Input type='time' {...props} />
);

export default TimeInput;
