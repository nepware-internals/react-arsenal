import React from 'react';

import Input from '../Input';

const DateTimeInput = (props) => (
    <Input type='datetime-local' {...props} />
);

export default DateTimeInput;
