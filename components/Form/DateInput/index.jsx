import React from 'react';

import Input from '../Input';

const DateInput =  (props) => (
    <Input type='date' {...props} />
);

export default DateInput;
