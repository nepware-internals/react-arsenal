import React from 'react';

import Input from '../Input';

const NumberInput =  (props) => (
    <Input type='number' {...props} />
);

export default NumberInput;
