import React from 'react';

import Input from '../Input';

const ColorInput = (props) => (
    <Input type='color' {...props} />
);

export default ColorInput;
