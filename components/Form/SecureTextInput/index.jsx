import React from 'react';

import Input from '../Input';

const SecureTextInput = (props) => (
    <Input type='password' {...props} />
);

export default SecureTextInput;
