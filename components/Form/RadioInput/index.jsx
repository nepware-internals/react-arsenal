import React from 'react';

import Input from '../Input';

const RadioInput =  (props) => {
    return (
        <Input type='radio' {...props} />
    );
};

export default RadioInput;
