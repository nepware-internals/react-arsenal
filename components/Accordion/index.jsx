
import React, {useCallback, useEffect, useRef, useState, useMemo} from 'react';
import PropTypes from 'prop-types';
import {FiChevronRight} from 'react-icons/fi';

import cs from '../../cs';

import styles from './styles.module.scss';
import useControlledState from '../../hooks/useControlledState';

const propTypes = {
    /*
     * Title of the accordion. Is clickable and opens the content.
     * Not used when renderHeader is passed.
     */
    title: PropTypes.string,
    /*
     * Custom renderer for the accordion header.
     * Called with isExpanded denoting whether accordion content is currently visible or not.
     */
    renderHeader: PropTypes.func,
    /*
     * Class applied to the container for the accordion.
     */
    className: PropTypes.string,
    /*
     * Class applied to the expanded accordion.
     */
    activeClassName: PropTypes.string,
    /*
     * Class applied to the title of the accordion when not custom rendered.
     */
    titleClassName: PropTypes.string,
    /*
     * Indicates default state of the accordion.
     * Does not take affect if accordion is controlled.
     */
    isExpandedByDefault: PropTypes.bool,
    /*
     * Controlled boolean indicating if the accordion is expanded.
     * If controlled, this value overrides the default state.
     */
    isExpanded: PropTypes.bool,
    children: PropTypes.any,
};

const Accordion = (props) => {
    const {
        isExpandedByDefault = false,
        isExpanded,
        title,
        children,
        className,
        activeClassName,
        renderHeader,
        titleClassName,
    } = props;

    const content = useRef();
    const [active, setActive] = useControlledState(isExpandedByDefault, {
        value: isExpanded
    });

    const [contentHeight, setContentHeight] = useState('0px');

    const toggleAccordion = useCallback(() => setActive((prev) => !prev), []);

    useEffect(() => {
        if (active && content.current) {
            setContentHeight(`${content.current.scrollHeight}px`);
        } else {
            setContentHeight('0px');
        }
    }, [active]);

    return (
        <div className={cs(styles.accordionSection, className, active && activeClassName)}>
            <div className={styles.accordion} onClick={toggleAccordion}>
                {renderHeader ? renderHeader({isExpanded: active}) : (
                    <div className={cs(styles.accordionTitle, titleClassName)}>
                        {title}
                        <FiChevronRight
                            className={cs(styles.rightIcon, {[styles.rotateUp]: active})}
                        />
                    </div>
                )}
            </div>
            <div
                ref={content}
                style={{maxHeight: contentHeight}}
                className={cs(styles.accordionContent, {
                    [styles.accordionContentActive]: active
                })}
            >
                {children}
            </div>
        </div>
    );
};

Accordion.propTypes = propTypes;

export default Accordion;
